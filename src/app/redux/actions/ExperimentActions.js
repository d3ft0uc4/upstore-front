import axios from "axios";
import history from 'history.js'
export const PARSE_URL_STORE = "PARSE_URL_STORE";
export const SUCCESS_PARSE_URL_STORE = "SUCCESS_PARSE_URL_STORE";

export function parseUrl({ link, language }) {
    return dispatch => {
        dispatch({
            type: PARSE_URL_STORE
        });

        axios.post("/stores", { existing_url: link, language }).then(res => {
            dispatch({
                type: SUCCESS_PARSE_URL_STORE,
                payload: res.data
            });
            history.push('/store/list')
        });
    };
}
