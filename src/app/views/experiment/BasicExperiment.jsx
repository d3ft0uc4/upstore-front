import React, {Component} from "react";
import {Breadcrumb} from "matx";
import SimpleForm from "../material-kit/forms/SimpleForm";
import {
    Button,
    Card,
    Checkbox,
    FormControlLabel,
    Grid,
    Icon,
    LinearProgress,
    Radio,
    RadioGroup, withStyles
} from "@material-ui/core";

import SimpleCard from "../../../matx/components/cards/SimpleCard";
import {SelectValidator, TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import MenuItem from "@material-ui/core/MenuItem";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import {parseUrl} from "../../redux/actions/ExperimentActions";
class BasicExperiment extends Component {

    state = {
        link: "",
        language: "en",
    };

    componentDidMount() {
        const regex = /^https:\/\/[a-z]*.apple.com\/[a-z]{2}\/app\/.*\/id[0-9]*$/
        ValidatorForm.addValidationRule("isLinkValid", value => {
            return regex.test(value)
        });
    }

    componentWillUnmount() {
        // remove rule when it is not needed
        ValidatorForm.removeValidationRule("isLinkValid");
    }

    handleSubmit = event => {
        console.log("submitted");
        this.props.parseUrl({ ...this.state });
        console.log(event);
    };

    handleChange = event => {
        event.persist();
        this.setState({[event.target.name]: event.target.value});
    };


    render() {
        let {
            link,
            language
        } = this.state;

        return (
            <div className="m-sm-30">
                <div className="mb-sm-30">
                    <Breadcrumb
                        routeSegments={[
                            {name: "Experiment", path: "/"},
                            {name: "Create"}
                        ]}
                    />
                </div>
                <ValidatorForm
                    ref="form"
                    onSubmit={this.handleSubmit}
                    onError={errors => null}
                >
                    <SimpleCard title="Provide an App Store link or enter the App title">
                        <Grid container spacing={6}>
                            <Grid item lg={12} md={12} sm={12} xs={12}>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="Link"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="link"
                                    value={link}
                                    validators={[
                                        "required",
                                        "isLinkValid"
                                    ]}
                                    errorMessages={["this field is required", "link is invalid"]}
                                />
                                <SelectValidator
                                    className="mb-4 max-w-770"
                                    label="Language"
                                    // onChange={this.handleChange}
                                    type="text"
                                    name="language"
                                    value={language}
                                    disabled
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                    <MenuItem value={'en'}>English</MenuItem>
                                </SelectValidator>
                            </Grid>
                        </Grid>
                    </SimpleCard>

                    <div className="py-3"/>
                    <Button color="primary" variant="contained" type="submit">
                        <Icon>send</Icon>
                        <span className="pl-2 capitalize">Create page</span>
                    </Button>
                </ValidatorForm>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    parseUrl: PropTypes.func.isRequired,
    link: state.link,
    language: state.language
});

export default withRouter(connect(mapStateToProps, { parseUrl })(BasicExperiment));
