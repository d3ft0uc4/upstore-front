import React from "react";

const experimentRoutes = [
  {
    path: "/experiment/create",
    component: React.lazy(() => import("./BasicExperiment"))
  }
];

export default experimentRoutes;
