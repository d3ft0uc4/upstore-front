import React, { Component } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import {DeleteOutlined} from "@material-ui/icons";

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 ${grid}px 0 0`,
  boxShadow: "var(--elevation-z4)",
  borderRadius: "4px",
  // change background colour if dragging
  background: isDragging ? "var(--primary)" : "var(--bg-paper)",

  // styles we need to apply on draggables
  ...draggableStyle
});

const imageStyle =  ({
  right: "20px",
  top: "20px"
});

const getListStyle = isDraggingOver => ({
  borderRadius: "4px",
  background: isDraggingOver ? "rgba(0,0,0, .1)" : "var(--bg-default)",
  display: "flex",
  padding: grid,
  overflow: "auto"
});

class SimpleHorizontalList extends Component {
  constructor(props) {
    super(props);
    this.onDragEnd = this.onDragEnd.bind(this);
  }

  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(
      this.props.items,
      result.source.index,
      result.destination.index
    );

    this.props.handleNewState(items)

  }

  deleteItem(index) {
    const items = Array.from(this.props.items);
    items.splice(index, 1)
    this.props.handleNewState(items)
  }


  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity
  render() {
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <Droppable droppableId="droppable" direction="horizontal">
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              style={getListStyle(snapshot.isDraggingOver)}
              {...provided.droppableProps}
            >
              {this.props.items.map((item, index) => (
                <Draggable key={`${item.link}${index}`} draggableId={`${item.link}${index}`} index={index}>
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      style={getItemStyle(
                        snapshot.isDragging,
                        provided.draggableProps.style
                      )}
                    >
                      {item.content}
                      {item.link && <div style={imageStyle}>
                      <img height={200} src={item.link} />
                        <DeleteOutlined fontSize="large"  onClick={() => this.deleteItem(index)}/>
                      </div> }
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    );
  }
}

export default SimpleHorizontalList;
