import React, {Component} from "react";
import {Breadcrumb} from "matx";
import SimpleForm from "../material-kit/forms/SimpleForm";
import {
    Button,
    Card,
    Checkbox,
    FormControlLabel,
    Grid,
    Icon,
    LinearProgress,
    Radio,
    RadioGroup
} from "@material-ui/core";
import SimpleCard from "../../../matx/components/cards/SimpleCard";
import {SelectValidator, TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import MenuItem from "@material-ui/core/MenuItem";
import SimpleHorizontalList from "../Drag&Drop/SimpleHorizontalList";
import SimpleRadio from "../material-kit/radio/SimpleRadio";
import {DropzoneArea, DropzoneAreaBase} from "material-ui-dropzone";
import Typography from "../utilities/Typography";
import Dropzone from "react-dropzone";
import {createStore, getStore, updateStore, uploadFile} from "./StoreService";
import {DeleteOutlined} from "@material-ui/icons";
import Loading from "../../../matx/components/MatxLoadable/Loading";
import Alert from "@material-ui/lab/Alert";
import MatxSnackbar from "../../../matx/components/MatxSnackbar";

class BasicStore extends Component {

    state = {
        id: "",
        screenshot_type: "iphone_x",
        version: "",
        developer: "",
        whats_new: "",
        store_name: "",
        title: "",
        subtitle: "",
        description: "",
        about: "",
        overall_rating: "",
        ratings: "",
        store_url: "",
        size: "",
        seller: "",
        language: "en",
        category: "",
        image_url: "",
        age: "",
        comments: [],
        images: [],
        images_iphone_x: [],
        images_iphone_8: [],
        images_ipad: [],
        ym: 0,
        click_link: ""
    };

    componentDidMount() {
        let {id: storeId} = this.props.match.params
        if (!storeId) {
            return
        }
        this.setState({id: storeId})

        getStore(storeId).then(({data}) => {
            if (!data) {
                this.props.history.push('/store/list')
                return
            }
            this.setState({...data})
            if (!this.state.store_name) {
                this.setState({store_name: this.state.title})
            }
            this.setState({images_iphone_x: data.images.filter(e => e.type === 'iphone_x')})
            this.setState({images_iphone_8: data.images.filter(e => e.type === 'iphone_8')})
            this.setState({images_ipad: data.images.filter(e => e.type === 'images_ipad')})
        }).catch(() => {
            this.props.history.push('/store/list')
            return
        })
    }

    componentWillUnmount() {

    }

    uploadImage = (image) => {
        uploadFile(image).then(({data}) => this.setState({image_url: data.url}))
    }



    uploadScreenshot = (images, type) => {
        const pushScreenshot = (data) => {
            this.setState({['images_' + type]: [data, ...this.state['images_' + type]]})
        }

        images.forEach(function (image) {
            uploadFile(image.file).then(({data}) => {
                    const _id = new Date().getTime()
                    pushScreenshot({link: data.url, id: _id, type: type})
                }
            )
        });
    }



    handleSubmit = event => {
        this.setState({...this.state, loading: true})
        const images = [...this.state.images_iphone_x,this.state.images_iphone_8,this.state.images_ipad]
        let tempState = {...this.state, images:images}
        delete tempState.loading
        if (!this.state.id)
            createStore(tempState).then(() => {
                this.setState({...this.state, loading: false})
                this.props.history.push(`/store/list`)
            })
        else
            updateStore(tempState).then(() => {
                this.setState({...this.state, loading: false, success: true})
            })
    };

    handleChange = event => {
        event.persist();
        this.setState({[event.target.name]: event.target.value});
    };

    handleScreenReordering = (items, type) => {
        this.setState({['images_' + type]: items})
    }

    render() {

        return (
            <div className="m-sm-30">

                <div className="mb-sm-30">
                    <Breadcrumb
                        routeSegments={[
                            {name: "Store", path: "/"},
                            {name: "Create"}
                        ]}
                    />
                </div>
                <ValidatorForm
                    ref="form"
                    onSubmit={this.handleSubmit}
                    onError={errors => null}
                >
                    <SimpleCard title="Store settings">
                        <Grid container spacing={6}>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="Store title"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="store_name"
                                    value={this.state.store_name}
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                />
                            </Grid>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <SelectValidator
                                    className="mb-4 w-full"
                                    label="Language"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="language"
                                    value={this.state.language}
                                    disabled
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                    <MenuItem value={'en'}>English</MenuItem>
                                </SelectValidator>
                            </Grid>
                        </Grid>
                    </SimpleCard>

                    <div className="py-3"/>

                    <SimpleCard title="General Information">
                        <Grid container spacing={6}>
                            <Grid item lgF={6} md={6} sm={12} xs={12}>
                                {this.state.image_url ? <>
                                    <img width={150} height={150} src={this.state.image_url}/>
                                    <DeleteOutlined fontSize="medium" onClick={() => this.setState({image_url: ""})}/>
                                </> : <DropzoneArea
                                    filesLimit={1}
                                    showPreviews={true}
                                    showAlerts={false}
                                    acceptedFiles={['image/*']}
                                    dropzoneText={"Choose a file or drop it here"}
                                    onDrop={(files) => this.uploadImage(files[0])}
                                />}

                            </Grid>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="App title"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="title"
                                    value={this.state.title}
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                />
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="App subtitle"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="subtitle"
                                    value={this.state.subtitle}
                                    validators={[]}
                                    errorMessages={["this field is required"]}
                                />
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="Developer"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="developer"
                                    value={this.state.developer}
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                />
                                <SelectValidator
                                    className="mb-4 w-full"
                                    label="Category"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="category"
                                    value={this.state.category}
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                    <MenuItem value={'Books'}>Books</MenuItem>
                                    <MenuItem value={'Medical'}>Medical</MenuItem>
                                    <MenuItem value={'Business'}>Business</MenuItem>
                                    <MenuItem value={'Music'}>Music</MenuItem>
                                    <MenuItem value={'Developer Tools'}>Developer Tools</MenuItem>
                                    <MenuItem value={'navigation'}>Navigation</MenuItem>
                                    <MenuItem value={'Education'}>Education</MenuItem>
                                    <MenuItem value={'News'}>News</MenuItem>
                                    <MenuItem value={'Entertainment'}>Entertainment</MenuItem>
                                    <MenuItem value={'Photo & Video'}>Photo & Video</MenuItem>
                                    <MenuItem value={'Finance'}>Finance</MenuItem>
                                    <MenuItem value={'Food & Drink'}>Food & Drink</MenuItem>
                                    <MenuItem value={'Productivity'}>Productivity</MenuItem>
                                    <MenuItem value={'Games'}>Games</MenuItem>
                                    <MenuItem value={'Reference'}>Reference</MenuItem>
                                    <MenuItem value={'Graphics & Design'}>Graphics & Design</MenuItem>
                                    <MenuItem value={'Safari Extensions'}>Safari Extensions</MenuItem>
                                    <MenuItem value={'Shopping'}>Shopping</MenuItem>
                                    <MenuItem value={'Health & Fitness'}>Health & Fitness</MenuItem>
                                    <MenuItem value={'Social Networking'}>Social Networking</MenuItem>
                                    <MenuItem value={'Lifestyle'}>Lifestyle</MenuItem>
                                    <MenuItem value={'Sports'}>Sports</MenuItem>
                                    <MenuItem value={'Kids'}>Kids</MenuItem>
                                    <MenuItem value={'Travel'}>Travel</MenuItem>
                                    <MenuItem value={'Magazines & Newspapers'}>Magazines & Newspapers</MenuItem>
                                    <MenuItem value={'Utilities'}>Utilities</MenuItem>
                                    <MenuItem value={'Weather'}>Weather</MenuItem>

                                </SelectValidator>
                            </Grid>
                        </Grid>
                    </SimpleCard>

                    <div className="py-3"/>

                    <SimpleCard title="Screenshots">
                        <RadioGroup
                            className="mb-4"
                            value={this.state.screenshot_type}
                            onChange={this.handleChange}
                            row>
                            <FormControlLabel
                                value="iphone_x"
                                onClick={() => this.setState({screenshot_type: 'iphone_x'})}
                                control={<Radio color="primary"/>}
                                label="iPhone X"
                                labelPlacement="end"
                            />
                            <FormControlLabel
                                value="iphone_8"
                                onClick={() => this.setState({screenshot_type: 'iphone_8'})}
                                control={<Radio color="primary"/>}
                                label="iPhone 8"
                                labelPlacement="end"
                            />
                            <FormControlLabel
                                value="ipad"
                                onClick={() => this.setState({screenshot_type: 'ipad'})}
                                control={<Radio color="primary"/>}
                                label="iPad"
                                labelPlacement="end"
                            />
                        </RadioGroup>
                        {this.state.screenshot_type === 'iphone_x' && <><DropzoneAreaBase
                            filesLimit={100}
                            acceptedFiles={['image/*']}
                            showPreviews={false}
                            showPreviewsInDropzone={false}
                            dropzoneText={"Drag and drop an image here or click"}
                            onAdd={(newFiles) => this.uploadScreenshot(newFiles,'iphone_x')}
                        />
                        <SimpleHorizontalList items={this.state.images_iphone_x} handleNewState={(items) => this.handleScreenReordering(items,'iphone_x')}/></>
                        }
                        {this.state.screenshot_type === 'iphone_8' && <><DropzoneAreaBase
                            filesLimit={100}
                            acceptedFiles={['image/*']}
                            showPreviews={false}
                            showPreviewsInDropzone={false}
                            dropzoneText={"Drag and drop an image here or click"}
                            onAdd={(newFiles) => this.uploadScreenshot(newFiles,'iphone_8')}
                        />
                            <SimpleHorizontalList items={this.state.images_iphone_8} handleNewState={(items) => this.handleScreenReordering(items,'iphone_8')}/></>
                        }
                        {this.state.screenshot_type === 'ipad' && <><DropzoneAreaBase
                            filesLimit={100}
                            acceptedFiles={['image/*']}
                            showPreviews={false}
                            showPreviewsInDropzone={false}
                            dropzoneText={"Drag and drop an image here or click"}
                            onAdd={(newFiles) => this.uploadScreenshot(newFiles,'ipad')}
                        />
                            <SimpleHorizontalList items={this.state.images_ipad} handleNewState={(items) => this.handleScreenReordering(items,'ipad')}/></>
                        }
                    </SimpleCard>

                    <div className="py-3"/>

                    <SimpleCard title="Description">
                        <Grid container spacing={6}>
                            <Grid item lg={12} md={12} sm={12} xs={12}>
                                <TextValidator
                                    multiline
                                    className="mb-4 w-full"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="about"
                                    value={this.state.about}
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                            </Grid>
                        </Grid>
                    </SimpleCard>

                    <div className="py-3"/>

                    <SimpleCard title="What's new">
                        <Grid container spacing={6}>
                            <Grid item lg={12} md={12} sm={12} xs={12}>
                                <TextValidator
                                    className="mb-4 w-full"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="whats_new"
                                    multiline
                                    value={this.state.whats_new}
                                    validators={[
                                        // "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="Version"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="version"
                                    value={this.state.version}
                                    validators={[
                                        // "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                            </Grid>
                        </Grid>
                    </SimpleCard>

                    <div className="py-3"/>

                    <SimpleCard title="Rating and reviews">
                        <Grid container spacing={6}>
                            <Grid item lg={2} md={2} sm={12} xs={12}>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="5 star"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="5star"
                                    validators={[
                                        // "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="4 star"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="4star"
                                    validators={[
                                        // "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="3 star"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="3star"
                                    validators={[
                                        // "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="2 star"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="2star"
                                    validators={[
                                        // "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="1 star"
                                    onChange={this.handleChange}
                                    type="text"
                                    name="1star"
                                    validators={[
                                        // "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                            </Grid>
                            <Grid item lg={4} md={4} sm={12} xs={12}>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="Overall rating"
                                    onChange={this.handleChange}
                                    type="number"
                                    min="0"
                                    name="overall_rating"
                                    value={this.state.overall_rating}
                                    validators={[
                                        // "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                                <TextValidator
                                    className="mb-4 w-full"
                                    label="Ratings"
                                    onChange={this.handleChange}
                                    type="number"
                                    min="0"
                                    name="ratings"
                                    value={this.state.ratings}
                                    validators={[
                                        // "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                            </Grid>
                        </Grid>
                    </SimpleCard>
                    <div className="py-3"/>

                    <SimpleCard title="Information">
                        <Grid container spacing={6}>
                            <Grid item lg={12} md={12} sm={12} xs={12}>
                                <TextValidator
                                    className="mb-4 w-full"
                                    onChange={this.handleChange}
                                    type="text"
                                    label="Seller"
                                    name="seller"
                                    multiline
                                    value={this.state.seller}
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                                <TextValidator
                                    className="mb-4 w-full"
                                    onChange={this.handleChange}
                                    type="text"
                                    label="Age"
                                    name="age"
                                    multiline
                                    value={this.state.age}
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                                <TextValidator
                                    className="mb-4 w-full"
                                    onChange={this.handleChange}
                                    type="text"
                                    label="Size"
                                    name="size"
                                    multiline
                                    value={this.state.size}
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                                <TextValidator
                                    className="mb-4 w-full"
                                    onChange={this.handleChange}
                                    type="number"
                                    min={0}
                                    step={1}
                                    label="Ya metrika"
                                    name="ym"
                                    multiline
                                    value={this.state.ym}
                                    validators={[
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                                <TextValidator
                                    className="mb-4 w-full"
                                    onChange={this.handleChange}
                                    type="text"
                                    label="Click link"
                                    name="click_link"
                                    multiline
                                    value={this.state.click_link}
                                    validators={[
                                        "required"
                                    ]}
                                    errorMessages={["this field is required"]}
                                >
                                </TextValidator>
                            </Grid>
                        </Grid>
                    </SimpleCard>
                    <div className="py-3"/>
                    <Button color="primary" variant="contained" type="submit">
                        <Icon>send</Icon>
                        <span className="pl-2 capitalize">Save</span>
                    </Button>
                </ValidatorForm>
                <br/>
            </div>
        )
    }
}

export default BasicStore;
