import React from "react";

const storeRoutes = [
  {
    path: "/store/create",
    component: React.lazy(() => import("./BasicStore"))
  },
  {
    path: "/store/list",
    component: React.lazy(() => import("./StoreList"))
  },
  {
    path: "/store/:id",
    component: React.lazy(() => import("./BasicStore"))
  }

];

export default storeRoutes;
