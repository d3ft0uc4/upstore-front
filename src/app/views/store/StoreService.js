import axios from 'axios'

export const getAllStores = () => {
    return axios.get('/stores')
}


export const getStore = (id) => {
    return axios.get(`/stores/${id}`)
}


export const createStore = ({ ...store }) => {
    return axios.post('/stores/', { ...store })
}

export const updateStore = ({ id, ...rest }) => {
    return axios.put(`/stores/${id}`, { ...rest })
}


export const deleteStore = (id) => {
    return axios.delete(`/stores/${id}`)
}


export const copyStore = (id) => {
    return axios.get(`/stores/duplicate/${id}`)
}

export const uploadFile = (file) => {
    const formData = new FormData();
    formData.append("file", file);
    return axios.post('/images/upload', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
}
