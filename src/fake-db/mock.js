const MockAdapter = require('axios-mock-adapter');

const axios = require('axios');

axios.defaults.baseURL = 'http://34.228.70.129/api/v1';
// axios.defaults.baseURL = 'http://localhost:8000/api/v1';

const Mock = new MockAdapter();
export default Mock;
