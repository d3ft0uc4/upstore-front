import React from "react";
import {Card, Switch} from "@material-ui/core";
import { classList } from "utils";

const SimpleCard = ({ children, title, subtitle, needSwitch, icon }) => {
  return (
    <Card elevation={6} className="px-6 py-5 h-full">
      <div
        className={classList({
          "card-title": true,
          "mb-4": !subtitle
        })}
      >
        {title}
          {needSwitch && <Switch
              className="sidenav__toggle show-on-pc"
              // onChange={this.handleSidenavToggle}
              checked={
                  false
              }
              color="secondary"
          />}
      </div>
      {subtitle && <div className="card-subtitle mb-4">{subtitle}</div>}

      {children}
    </Card>
  );
};

export default SimpleCard;
